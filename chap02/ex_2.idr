module ex2

palindrome : String -> Bool
palindrome str = toLower str == reverse (toLower str)

palindrome2 : Nat -> String -> Bool
palindrome2 l s =
  if length s > l then
    palindrome s
  else
    False

counts : String -> (Nat, Nat)
counts str = (length (words str), length str)

top_ten : Ord a => List a -> List a
top_ten xs = take 10 (reverse (sort xs))


over_length : Nat -> List String -> Nat
over_length n strs = length (filter (> n) (map length strs))
