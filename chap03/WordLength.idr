allLengths : List String -> List Nat
allLengths [] = []
allLengths (word :: words) = length word :: allLengths words


xor : Bool -> Bool -> Bool
xor False y = y
xor True y = not y


isEven1 : Nat -> Bool
isEven1 Z = True
isEven1 (S k) = not (isEven1 k)

mutual
  isEven : Nat -> Bool
  isEven Z = True
  isEven (S k) = isOdd k

  isOdd : Nat -> Bool
  isOdd Z = False
  isOdd (S k) = isEven k
