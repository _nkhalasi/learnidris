import Data.Vect

Matrix : Nat -> Nat -> Type -> Type
Matrix rows cols t = Vect rows (Vect cols t)


total addTwoVectors : Num elem => Vect sz elem -> Vect sz elem -> Vect sz elem
addTwoVectors [] [] = []
addTwoVectors (x :: xs) (y :: ys) = x + y :: addTwoVectors xs ys


total addMatrix : Num nt => Matrix r c nt -> Matrix r c nt -> Matrix r c nt
addMatrix [] [] = []
addMatrix [[]] [[]] = [[]]
addMatrix (m1Row :: m1Rows) (m2Row :: m2Rows) =
  (addTwoVectors m1Row m2Row) :: (addMatrix m1Rows m2Rows)



createEmpties : Vect n (Vect 0 elem)
createEmpties = replicate _ []

transposeHelper : (xs : Vect n elem) -> (ys : Vect n (Vect k elem)) -> Vect n (Vect (S k) elem)
transposeHelper [] [] = []
transposeHelper (x :: xs) (y :: ys) = (x :: y) :: transposeHelper xs ys

transposeMat : Vect m (Vect n elem) -> Vect n (Vect m elem)
transposeMat [] = createEmpties
transposeMat (x :: xs) = let xsTrans = transposeMat xs in
                            transposeHelper x xsTrans
