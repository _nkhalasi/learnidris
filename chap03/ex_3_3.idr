import Data.Vect

{- 1 -}

createEmpties : Vect n (Vect 0 elem)
createEmpties = replicate _ []

transposeMat : Vect m (Vect n elem) -> Vect n (Vect m elem)
transposeMat [] = createEmpties
transposeMat (curRow :: remainRows) = let rowsTrans = transposeMat remainRows in
                            zipWith (::) curRow rowsTrans

{- 2 -}

addMatrix : Num a => Vect n (Vect m a) -> Vect n (Vect m a) -> Vect n (Vect m a)
addMatrix [] [] = []
addMatrix (m1CurRow :: m1RemainingRows) (m2CurRow :: m2RemainingRows) =
  zipWith (+) m1CurRow m2CurRow :: addMatrix m1RemainingRows m2RemainingRows

{- 3 -}

multVecs : Num a => (xs : Vect m a) -> (ys : Vect m a) -> a
multVecs xs ys = sum (zipWith (*) xs ys)

makeRow : Num a => (x : Vect m a) -> (m2Trans : Vect p (Vect m a)) ->  Vect p a
makeRow x [] = []
makeRow x (y :: xs) = multVecs x y :: makeRow x xs

multMatrixHelper : Num a => (m1 : Vect n (Vect m a)) -> (m2Trans : Vect p (Vect m a)) -> Vect n (Vect p a)
multMatrixHelper [] m2Trans = []
multMatrixHelper (x :: xs) m2Trans = makeRow x m2Trans :: multMatrixHelper xs m2Trans

multMatrix : Num a => Vect n (Vect m a) -> Vect m (Vect p a) -> Vect n (Vect p a)
multMatrix [] [] = []
multMatrix m1 m2 = let m2Trans = transposeMat m2 in
                      multMatrixHelper m1 m2Trans
