import Data.Vect

append : (elem: Type) -> (n: Nat) -> (m : Nat) -> Vect n elem -> Vect m elem -> Vect (n+m) elem
append elem Z m [] ys = ys
append elem (S k) m (x :: xs) ys = x :: append elem k m xs ys


{- Length of Vector without using implicit args -}
len : Vect n elem -> Nat
len [] = 0
len (x :: xs) = 1 + len xs

{- Length of Vector using implicit args -}
lenImpl : Vect n elem -> Nat
lenImpl {n} xs = n


createEmpties : Vect n (Vect 0 a)
createEmpties {n = Z} = []
createEmpties {n = (S k)} = [] :: createEmpties
-- createEmpties {a=Char} {n=5}
