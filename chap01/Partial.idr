module Partial

add : Int -> Int -> Int
add x y = x + y

doubleIt : Int -> Int
doubleIt x = add x x

double : Num ty => ty -> ty
double x = x + x

-- Higher Order Function
twice : (a -> a) -> a -> a
twice f x = f (f x)

-- Partially applied function using HOF
quadruple : Num a => a -> a
quadruple = twice double

Shape: Type
rotate : Shape -> Shape

-- Partially applied function using HOF
turn_around : Shape -> Shape
turn_around = twice rotate
