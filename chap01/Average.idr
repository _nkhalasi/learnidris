module Average

||| Calculate the average length of words in a string
||| @str A string containing words separated by whitespaces
export
average : (str: String) -> Double
average str =
  let numWords = wordCount str
      totalLength = sum (allLengths (words str)) in
      cast totalLength / cast numWords
  where
    wordCount : String -> Nat
    wordCount str = length (words str)

    allLengths : List String -> List Nat
    allLengths strs = map length strs
