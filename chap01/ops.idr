module Ops

||| Add two natural numbers.
add : Nat -> Nat -> Nat
add Z  y = y
add (S k) y = S(add k y)

||| Multiply two natural numbers.
mul : Nat -> Nat -> Nat
mul Z y = Z
mul (S k) y = add y (mul k y)


identity : a -> a
identity x = x
