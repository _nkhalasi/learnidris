printLength : IO ()
printLength = putStr "Enter Input String : " >>=
                (\_ => getLine) >>=
                (\input => let len = length input in
                                  putStrLn (show len))

{-
Syntactic sugar for sequencing with do notation
Interactive programs are, by their nature, typically imperative in style, with a sequence
of commands, each of which produces a value that can be used later. The >>= function
captures this idea, but the resulting definitions can be difficult to read.
-}
printLength2 : IO ()
printLength2 = do
        putStr "Enter Input String: "
        input <- getLine
        -- putStrLn (show (length input))
        let len = length input
        putStrLn (show len)
