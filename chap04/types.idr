module Types

import Data.Vect

-- Enumerated types - defined by giving possible values directly
data Bool = False | True
data Direction = North
                | East
                | South
                | West

turnClockwise : Direction -> Direction
turnClockwise North = East
turnClockwise East = South
turnClockwise South = West
turnClockwise West = North

-- Union types - Enum types carrying additional data with each value
||| Represents shapes
public export
data Shape = ||| A triangle with its base and height
          Triangle Double Double
          | ||| A rectangle with its length and heigh
          Rectangle Double Double
          | ||| A circle with its radius
          Circle Double

export
area : Shape -> Double
area (Triangle base height) = 0.5 * base * height
area (Rectangle length height) = length * height
area (Circle radius) = pi * radius * radius

-- Recursive types - Union types defined in terms of themselves
-- e.g. data Nat = Z | S Nat

public export
data Picture : Type where
  ||| A Primitive shape
  Primitive : Shape -> Picture
  ||| A combination of two other pictures
  Combine : Picture -> Picture -> Picture
  ||| A picture rotated through an angle
  Rotate: Double -> Picture -> Picture
  ||| A picture translated to a different location
  Translate: Double -> Double -> Picture -> Picture

export
rectangle : Picture
rectangle = Primitive (Rectangle 20 10)

export
circle : Picture
circle = Primitive (Circle 5)

export
triangle : Picture
triangle = Primitive (Triangle 10 10)

export
testPicture : Picture
testPicture = Combine (Translate 5 5 rectangle)
              (Combine (Translate 35 5 circle)
                       (Translate 15 25 triangle))

export
pictureArea : Picture -> Double
pictureArea (Primitive shape) = area shape
pictureArea (Combine pic1 pic2) = pictureArea pic1 + pictureArea pic2
pictureArea (Rotate angle pic) = pictureArea pic
pictureArea (Translate x y pic) = pictureArea pic


-- Generic types - parameteraized over some other types

data DivResult = DivByZero | Result Double
safeDivide : Double -> Double -> DivResult
safeDivide x y = if y == 0 then DivByZero else Result (x/y)

data May_be valType = Nothing | Just valType
safeDivide2 : Double -> Double -> May_be Double
safeDivide2 x y = if y == 0 then Nothing else Just (x/y)


-- Dependent types - computed from some other value

data PowerSource = Petrol | Pedal

data Vehicle : PowerSource -> Type where
  Bicycle : Vehicle Pedal
  Car : (fuel: Nat) -> Vehicle Petrol
  Bus : (fuel: Nat) -> Vehicle Petrol

wheels: Vehicle power -> Nat
wheels Bicycle = 2
wheels (Car fuel) = 4
wheels (Bus fuel) = 4

refuel: Vehicle Petrol -> Vehicle Petrol
refuel (Car fuel) = Car 100
refuel (Bus fuel) = Bus 200
refuel Bicycle impossible


------
tryIndex : Integer -> Vect n a -> Maybe a
tryIndex {n} i xs = case integerToFin i n of
                        Nothing => Nothing
                        Just idx => Just (index idx xs)
