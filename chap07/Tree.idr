data Tree elem = Empty | Node (Tree elem) elem (Tree elem)

Eq elem => Eq (Tree elem) where
  (==) Empty Empty = True
  (==) (Node left e right) (Node left' e' right') =
          left == left' && e == e' && right == right'
  (==) _ _ = False

Functor Tree where
  map func Empty = Empty
  map func (Node left e right) =
        Node (map func left) (func e) (map func right)

Foldable Tree where
  foldr func acc Empty = acc
  foldr func acc (Node left e right) =
          let leftFold = foldr func acc left
              rightFold = foldr func leftFold right in
              func e rightFold
