label : List a -> List (Integer, a)
labelFrom : Integer -> List a -> List (Integer, a)
labelFrom idx [] = []
labelFrom idx (x :: xs) = (idx, x) :: labelFrom (idx+1) xs

label = labelFrom 0


||| Non-terminating (and hence not total)
countFrom : Integer -> List Integer
countFrom n = n :: countFrom (n+1)


labelWith : Stream labelType -> List a -> List (labelType, a)
labelWith lbls [] = []
labelWith (lbl :: lbls) (val :: vals) = (lbl, val) :: labelWith lbls vals

label' : List a -> List (Integer, a)
label' vals = labelWith (iterate (+1) 0) vals
