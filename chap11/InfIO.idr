data InfIO : Type where
  Do : IO a -> (a -> Inf InfIO) -> InfIO


loopPrint : String -> InfIO
loopPrint msg = Do (putStrLn msg) (\_ => loopPrint msg)

-- run : InfIO -> IO ()
-- run (Do action cont) = do res <- action
--                           run (cont res)

data Fuel = Dry | More Fuel

tank : Nat -> Fuel
tank Z = Dry
tank (S k) = More (tank k)

run : Fuel -> InfIO -> IO ()
run (More fuel) (Do action cont) = do res <- action
                                      run fuel (cont res)

run Dry p = putStrLn "Out of fuel!"
