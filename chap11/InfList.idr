data InfList : Type -> Type where
  (::) : (value : elem) -> Inf (InfList elem) -> InfList elem

%name InfList xs, ys, zs

{-
functions such as countFrom as corecursive rather than recursive,
and infinite lists as codata rather than data. The distinction between
data and codata is that data is finite and is intended to be consumed,
whereas codata is potentially infinite and is intended to be produced.
-}

countFrom : Integer -> InfList Integer
countFrom x = x :: Delay (countFrom (x+1))

getPrefix : (count : Nat) -> InfList a -> List a
getPrefix Z xs = []
-- getPrefix (S k) (value :: xs) = value :: getPrefix k (Force xs)
getPrefix (S k) (value :: xs) = value :: getPrefix k xs


countFrom : Integer -> InfList Integer
countFrom x = x :: Delay (countFrom (x + 1))

labelWith : InfList Integer -> List a -> List (Integer, a)
labelWith (lbl :: lbls) [] = []
labelWith (lbl :: lbls) (val :: vals) = (lbl, val) :: labelWith lbls vals

label : List a -> List (Integer, a)
label = labelWith (countFrom 0)
